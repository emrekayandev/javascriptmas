/* Panic function 
Write a PANIC! function. The function should take in a sentence and return the same
sentence in all caps with an exclamation point (!) at the end. Use JavaScript's
built in string methods. 

If the string is a phrase or sentence, add a 😱 emoji in between each word. 

Example input: "Hello"
Example output: "HELLO!"

Example input: "I'm almost out of coffee"
Example output: "I'M 😱 ALMOST 😱 OUT 😱 OF 😱 COFFEE!"
*/

// Takes in a word/sentence, and returns an uppercase version of it.
function capitalizeWords(word) {
  return word.toUpperCase();
}

// Accepts a sentence/text/word and checks if this a sentence, or just a word
// Returns boolean.
function isSentence(text) {
  return text.includes(" ");
}

// Takes in a sentence and Adds/returns Face Screaming in Fear emoji
function addFaceScreamingEmoji(sentence) {
  let words = sentence.split(" ");
  words = words.join(" 😱 ");
  return words;
}

// Takes in a sentence/word, returns either an emoji-added all-uppercase with an exclamation mark , or all uppercase version with an exclamation mark at the end of it.
function panic(sentence) {
  // Check if this a sentence, or just a word.
  if (isSentence(sentence)) {
    return `${capitalizeWords(addFaceScreamingEmoji(sentence))}!`;
  } else {
    return `${capitalizeWords(sentence)}!`;
  }
}

// Test your function
console.log(panic("Hello"));
console.log(panic("I'm almost out of coffee"));
console.log(panic("winter is coming"));
